from nltk.corpus import wordnet
from nltk.stem import WordNetLemmatizer
from dependency import get_text_list, find_node, find_node_by_lemma
from parsing_questions import (when_find_question_verb, when_find_question_subject,
    where_find_question_verb, where_find_question_subject, what_find_question_verb,
    what_find_question_subject, did_find_question_verb, did_find_question_subject)


def dependency_relation_list(dependencies):
    return [node for node in dependencies["deps"].keys()]


def print_color(color, text):
    # https://stackoverflow.com/questions/287871/print-in-terminal-with-colors
    colors = {"green":'\033[92m', "red":'\033[91m', 'orange': '\033[93m'}

    return colors[color] + text + '\033[0m'


def lemmatizer(verb, type):
    """
    Finds the root word for a given word.
    :param verb: string to find the root of
    :param type: word type (noun, verb, etc)
    :return: string
    """
    wordnet_lemmatizer = WordNetLemmatizer()
    lemmatize = wordnet_lemmatizer.lemmatize(verb, type)

    return lemmatize



def get_hypernyms(word):
    words = []

    for ss in wordnet.synsets(word):
        for s in ss.hypernyms():
            if s:
                words.append(s.name().split(".")[0])
    return words


def get_lemmas(word, word_type):
    words = []

    for ss in wordnet.synsets(word):
        for s in ss.hypernyms():
            for x in s.lemmas():
                if x.name() not in words:
                    words.append(x.name())
    return words


def sentence_detection(subject, verb, dependency_graph, all_text):
    # if subject is None:
    #     print("[*] Question Subject: ", print_color("red", "None"))
    # else:
    #     if len(subject) > 1:
    #         print("[*] Question Subject: ", print_color("orange", str(subject)))
    #     else:
    #         print("[*] Question Subject: ", print_color("red", str(subject)))

    # if verb is None:
    #     print("[*] Question Verb: ", print_color("red", "None"))
    # else:
    #     if len(verb) > 1:
    #         print("[*] Question Verb: ", print_color("orange", str(verb)))
    #     else:
    #         print("[*] Question Verb: ", print_color("red", str(verb)))

    for d in dependency_graph:
        s_found = False
        v_found = False
        verb_2_subject_0 = False
        verb_1_subject_0 = False
        verb_0_subject_1 = False
        verb_0_subject_2 = False
        s = []
        v = []

        for node in d.nodes.values():
            tag = node["tag"]
            word = node["word"]

            if verb is None and subject:
                subject_keys = subject.keys()
                subject_length = len(subject)
                if subject_length == 1:
                    subject_word = subject[list(subject_keys)[0]]
                    if word is not None:
                        if subject_word == word:
                            verb_0_subject_1 = True
                elif subject_length == 2:
                    if word is not None:
                        if subject["left"] == word or subject["right"] == word:
                            verb_0_subject_2 = True

            elif verb and subject:
                verb_keys = verb.keys()
                subject_keys = subject.keys()
                verb_length = len(verb)
                subject_length = len(subject)

                if verb_length == 1 and subject_length == 1:
                    subject_word = subject[list(subject_keys)[0]]
                    verb_word = verb[list(verb_keys)[0]]

                    # print("^^^^^^^^^^^^^^^^", subject_word, verb_word)

                    if word is not None:
                        if tag.startswith("V"):
                            v.append(word)
                            if verb_word == word:
                                v_found = True
                            elif lemmatizer(verb_word, "v") == lemmatizer(word, "v"):
                                v_found = True
                        else:
                            s.append(word)
                            if subject_word == word:
                                s_found = True
                elif verb_length == 2 and subject_length == 1:
                    pass
            elif verb and subject is None:
                verb_keys = verb.keys()
                verb_length = len(verb)
                verb_word = verb["first_verb"]

                if verb_length == 2:
                    if word is not None:
                        if tag.startswith("V"):
                            if lemmatizer(verb_word, "v") == lemmatizer(word, "v"):
                                # print("##############>>>>", verb_word, word)
                                verb_2_subject_0 = True
                elif verb_length == 1:
                    if word is not None:
                        # if tag.startswith("V"):
                        # print("*********>>> ", lemmatizer(verb_word, "v"), lemmatizer(word.lower(), "v"))
                        if lemmatizer(verb_word, "v") == lemmatizer(word.lower(), "v"):
                            verb_1_subject_0 = True


        # if len(s) > 0:
        #     print(s)
        # if len(v) > 0:
        #     print(v)
        if verb_0_subject_1:
            # print(print_color("green", "[*] Sentence: " + " ".join(get_text_list(d))))
            return d
        elif verb_0_subject_2:
            # print(print_color("green", "[*] Sentence: " + " ".join(get_text_list(d))))
            return d
        elif verb_2_subject_0:
            # print(print_color("green", "[*] Sentence: " + " ".join(get_text_list(d))))
            return d
        elif verb_1_subject_0:
            # print(print_color("green", "[*] Sentence: " + " ".join(get_text_list(d))))
            return d
        elif v_found and s_found:
            # print(print_color("green", "[*] Sentence: " + " ".join(get_text_list(d))))
            return d





            



        # sentence = []
        # for node in d.nodes.values():
        #     if node["word"]:
        #         sentence.append(node["word"].lower())

        # lemma_sentence = []
        # for node in d.nodes.values():
        #     if node["word"]:
        #         if node["tag"][0] == "V":
        #             lemma_sentence.append(lemmatizer(node["word"].lower(), "v"))
        #         else:
        #             lemma_sentence.append(node["word"].lower())

        # if verb and subject:
        #     verb_keys = verb.keys()
        #     subject_keys = subject.keys()
        #     verb_length = len(verb)
        #     subject_length = len(subject)

        #     if verb_length == 1 and subject_length == 1:
        #         subject_word = subject[list(subject_keys)[0]]
        #         verb_word = verb[list(verb_keys)[0]]


                # # lemma(word) <-> lemma(word)
                # if lemmatizer(verb_word, "v") in lemma_sentence and subject_word.lower() in sentence:
                #     print(print_color("green", "[*] Sentence: " + " ".join(get_text_list(d))))
                #     return d
                # # word <-> word
                # elif verb_word in sentence and subject_word.lower() in sentence:
                #     print(print_color("green", "[*] Sentence: " + " ".join(get_text_list(d))))
                #     return d
                # else:
                #     print("***** ", verb_word)
                #     print("*****: ", lemma_sentence)
                #     print("*****: ", sentence)



        # if s_found and v_found:
        #     print(print_color("green", "[*] Sentence: " + " ".join(get_text_list(d))))


def get_question_verb_subject(question_type, question):
    verb = ""
    subject = ""

    if question_type == "when":
        verb = when_find_question_verb(question)
        subject = when_find_question_subject(question)
    elif question_type == "where":
        verb = where_find_question_verb(question)
        subject = where_find_question_subject(question)
    elif question_type == "what":
        verb = what_find_question_verb(question)
        subject = what_find_question_subject(question)
    elif question_type == "did":
        verb = did_find_question_verb(question)
        subject = did_find_question_subject(question)
    return verb, subject


def parse_question_by_type(question_type, question_version, question, story):
    verb, subject = get_question_verb_subject(question_type, question)

    if question_version.lower() == "story":
        story_text = story["text"]
        story_dependency_graph = story["story_dep"]

        return sentence_detection(subject, verb, story_dependency_graph, story_text)
    elif question_version.lower() == "sch":
        sch_text = story["sch"]
        sch_dependency_graph = story["sch_dep"]

        return sentence_detection(subject, verb, sch_dependency_graph, sch_text)


def parse_sentence(question_type, question, story):
    question_text = question["text"]
    question_version = question["type"]
    # print("==================")
    # print("[*] Question: ", question_text)
    if question_version == "Sch":
        # print("[*] Question Type: ", question_version)
        return parse_question_by_type(question_type, question_version, question, story)
    elif question_version == "Story":
        # print("[*] Question Type: ", question_version)
        return parse_question_by_type(question_type, question_version, question, story)
    elif question_version == "Story | Sch":
        # print("[*] Question Type: ", question_version)
        # print("Story | Sch parsing_question.py")
        # parse_question_by_type(question_type, "Sch", question, story)
        return parse_question_by_type(question_type, "Story", question, story)
    elif question_version == "Sch | Story":
        # print("[*] Question Type: ", question_version)
        # parse_question_by_type(question_type, "Story", question, story)
        # print("Sch | Story parsing_question.py")
        return parse_question_by_type(question_type, "Sch", question, story)
