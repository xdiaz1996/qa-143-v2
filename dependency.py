#!/usr/bin/env python

import re, sys, nltk, operator
from nltk.stem import WordNetLemmatizer

def get_lemma_list(question_dependency_graph):
    tagged_question = []
    get_nodes = [node for node in question_dependency_graph.nodes.values() if node["address"] is not None]
    sorted_nodes = sorted(get_nodes, key=operator.itemgetter('address'))

    for node in sorted_nodes:
        # None is the root so we exclude it
        if node["lemma"] is not None:
            tagged_question.append(node["lemma"])
    return tagged_question


def get_text_list(question_dependency_graph):
    tagged_question = []
    get_nodes = [node for node in question_dependency_graph.nodes.values() if node["address"] is not None]
    sorted_nodes = sorted(get_nodes, key=operator.itemgetter('address'))

    for node in sorted_nodes:
        # None is the root so we exclude it
        if node["word"] is not None:
            tagged_question.append(node["word"])
    return tagged_question


def get_graph_pos(question_dependency_graph):
    tagged_question = []
    get_nodes = [node for node in question_dependency_graph.nodes.values()]
    sorted_nodes = sorted(get_nodes, key=operator.itemgetter('address'))

    for node in sorted_nodes:
        # None is the root so we exclude it
        if node["word"] is not None:
            tagged_question.append((node["word"], node["tag"]))
    return tagged_question


def find_main(graph):
    for node in graph.nodes.values():
        if node['rel'] == 'root':
            return node
    return None


def find_node(word, graph):
    for node in graph.nodes.values():
        if node["word"] == word:
            return node
    return None
    # type_name = type(word).__name__
    #
    # if type_name == "str":
    #     for node in graph.nodes.values():
    #         if node["word"] == word:
    #             return node
    #     return None
    # elif type_name == "list":
    #     # xxx = " ".join([node["word"] for node in graph.nodes.values() if node["word"]])
    #     # if all(w in xxx for w in word):
    #     #     print(yyy, xxx)
    #     for node in graph.nodes.values():
    #         if node["word"] == word[0]:
    #             return node
    #     return None



# def find_node_by_lemma(word, graph):
#     wordnet_lemmatizer = WordNetLemmatizer()
#
#     for node in graph.nodes.values():
#         answer_lemma = ""
#         # only lemmatize verbs
#         if node["lemma"] is not None and node["tag"][0] == "V":
#             answer_lemma = wordnet_lemmatizer.lemmatize(node["lemma"], "v")
#             # print(">>>>AL: ", answer_lemma)
#         if node["lemma"] is not None and word == answer_lemma:
#             return node
#         # else case when its irregular woke/wake go/went begin/began
#     return None

def find_node_by_lemma(word, graph):
    for node in graph.nodes.values():
        if node["lemma"] == word:
            return node
    return None


def find_node_by_tag(tag, graph):
    for node in graph.nodes.values():
        if node["tag"] == tag:
            return node
    return None


def find_node_by_address(address, graph):
    for node in graph.nodes.values():
        if node["address"] == address:
            return node
    return None


def find_node_by_rel(rel, graph):
    for node in graph.nodes.values():
        if node["rel"] == rel:
            return node
    return None


def get_dependents(node, graph):
    results = []
    for item in node["deps"]:
        address = node["deps"][item][0]
        dep = graph.nodes[address]
        results.append(dep)
        results = results + get_dependents(dep, graph)
        
    return results


def get_relation(node, sgraph):
    deps = get_dependents(node, sgraph)
    deps = sorted(deps+[node], key=operator.itemgetter("address"))
    return " ".join(dep["word"] for dep in deps)


def find_answer(qgraph, sgraph):
    qmain = find_main(qgraph)

    qword = ""
    lmtzr = WordNetLemmatizer()
    for node in sgraph.nodes.values():
        tag = node["tag"]
        word = node["word"]
        if word is not None:
            if tag.startswith("V"):
                # print("#^^^^^^##", lmtzr.lemmatize(qmain["word"], 'v'), lmtzr.lemmatize(word, 'v'))
                if lmtzr.lemmatize(qmain["word"], 'v') == lmtzr.lemmatize(word, 'v'):
                    qword = word

    # print("<<##########>>: ", qword)
    snode = find_node(qword, sgraph)
    # print(">>>>>>", snode)
    if snode:
        for node in sgraph.nodes.values():
            # added snode and
            # print("<<<<<<<<<<<<<", snode)
            if node.get('head', None) == snode["address"]:
                # print(">>>>", node['rel'])
                if node['rel'] == "nmod":
                    return get_relation(node, sgraph)
                elif node['rel'] == "compound":
                    return get_relation(node, sgraph)
                elif node['rel'] == "ccomp":
                    return get_relation(node, sgraph)
                elif node['rel'] == "xcomp":
                    return get_relation(node, sgraph)
                elif node['rel'] == "dobj":
                    return get_relation(node, sgraph)
                
    return None

# def find_answer(qgraph, sgraph):
#     qmain = find_main(qgraph)
#     qword = qmain["word"]

#     snode = find_node(qword, sgraph)

#     if snode:
#         for node in sgraph.nodes.values():
            # print(node["head"], snode["address"])
#             if node.get('head', None) == snode["address"]:
#                 print(node["word"], node["rel"])

#                 if node['rel'] == "nmod":
#                     deps = get_dependents(node, sgraph)
#                     deps = sorted(deps+[node], key=operator.itemgetter("address"))

#                     return " ".join(dep["word"] for dep in deps)


if __name__ == '__main__':
    pass