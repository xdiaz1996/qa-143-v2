from dependency import find_main, get_dependents, find_node_by_address

'''
['__class__', '__delattr__', '__dict__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', 
'__getattribute__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__le__', '__lt__', 
'__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', 
'__str__', '__subclasshook__', '__unicode__', '__weakref__', '_hd', '_parse', '_rel', '_repr_svg_', '_tree', 
'_word', 'add_arc', 'add_node', 'connect_graph', 'contains_address', 'contains_cycle', 'get_by_address', 
'get_cycle_path', 'left_children', 'load', 'nodes', 'nx_graph', 'redirect_arcs', 'remove_by_address', 'right_children', 
'root', 'to_conll', 'to_dot', 'top_relation_label', 'tree', 'triples', 'unicode_repr']
'''
# print(question_graph["dep"].root)
# print(question_graph["dep"].contains_address(1))
# print(question_graph["dep"].get_by_address(1))
# print(question_graph["dep"].left_children(2))
# print(question_graph["dep"].right_children(1))
# print(question_graph["dep"].top_relation_label)
# print(question_graph["dep"].top_relation_label)

def where_find_question_subject(question_graph):
    """
    Finds the subject noun from a question graph.
    :param question_graph: question graph
    :return: string
    """
    question_dep = find_main(question_graph["dep"])
    dependency_relation = [x for x in question_dep["deps"].keys()]
    deps = get_dependents(question_dep, question_graph["dep"])

    if "nsubj" in dependency_relation:
        for d in deps:
            if d["rel"] == "nsubj":
                return d["word"]
    elif "nsubjpass" in dependency_relation:
        for d in deps:
            if d["rel"] == "compound":
                return d["word"]


def where_find_question_verb(question_graph):
    """
    Finds the verb from a question graph.
    :param question_graph: question graph
    :return: string
    """
    question_dep = find_main(question_graph["dep"])
    dependency_relation = [x for x in question_dep["deps"].keys()]

    if question_dep["tag"] == "VBD":
        # <WRB> -(advmod)-> <VBD>
        return question_dep["word"]
    elif question_dep["tag"] == "VBG":
        # <WRB> -(advmod)-> <VBG>
        return question_dep["word"]
    elif question_dep["tag"] == "NN":
        deps = question_dep["deps"]

        # if "cop" in deps.keys():
            # print("********", deps)
            # print(">>>>>>>>", deps['cop'])

    # if dependency_relation[0] == "advmod":
    #     return question_dep["word"]
    # else:
    #     pass


def when_find_question_subject(question_graph):
    """
    Finds the subject noun from a question graph.
    :param question_graph: question graph
    :return: string
    """
    question_dep = find_main(question_graph["dep"])
    dependency_relation = [x for x in question_dep["deps"].keys()]
    deps = get_dependents(question_dep, question_graph["dep"])

    # print(">>>>>>>", dependency_relation)
    if "nsubj" in dependency_relation:
        for d in deps:
            if d["rel"] == "nsubj":
                return d["word"]
    elif "nsubjpass" in dependency_relation:
        for d in deps:
            if d["rel"] == "nsubjpass":
                return d["word"]


def when_find_question_verb(question_graph):
    """
    Finds the verb from a question graph.
    :param question_graph: question graph
    :return: string
    """
    question_dep = find_main(question_graph["dep"])

    return question_dep["word"]


def why_find_question_subject(question_graph):
    """
    Finds the subject noun from a question graph.
    :param question_graph: question graph
    :return: string
    """
    question_dep = find_main(question_graph["dep"])
    deps = get_dependents(question_dep, question_graph["dep"])
    subject = [dep["word"] for dep in deps if dep["rel"] == "nsubj" or dep["rel"] == "compound"]

    return subject[0]


def why_find_question_verb(question_graph):
    """
    Finds the verb from a question graph.
    :param question_graph: question graph
    :return: string
    """
    question_dep = find_main(question_graph["dep"])
    dependency_relation = [x for x in question_dep["deps"].keys()]

    if dependency_relation[0] == "advmod":
        if "amod" in question_dep["deps"].keys():
            # <WRB> (advmod)-> <NNS|NN|VB|VBP> (amod)-> <VBG>
            amod_index = question_dep["deps"]["amod"][0]
            deps = get_dependents(question_dep, question_graph["dep"])

            return deps[amod_index - 1]["word"]
        else:
            # <WRB> (advmod)-> <NNS|NN|VB|VBP>
            return question_dep["word"]
    elif dependency_relation[0] == "dep":
        deps = get_dependents(question_dep, question_graph["dep"])

        # <WRB> (dep)-> <VBD> (nsubj)-> <NNP> (nmod)-> <NN>
        for d in deps:
            if d["rel"] == "nmod":
                return d["word"]


# def what_find_question_verb(question_graph):
    # head_node = question_graph["dep"].nodes[1] # first word in question
    # # root is the word all the dependencies stem from
    # root_node = question_graph["dep"].root ## returns the {root node}
    # root_node_dependencies = root_node["deps"] ## returns the dependencies list [dobj, deps stc]

    # if head_node["tag"] == "WP":
    #     # print("WP")

    #     # print(">>>", head_node["rel"])
    #     # print(">>> head node: ", head_node)
    #     # print(">>> root deps", root_node_dependencies)
    #     # print(">>> root node", root_node)

    #     if head_node["rel"] == "dobj":
    #         advcl_index = root_node_dependencies["advcl"]

    #         if advcl_index:
    #             return question_graph["dep"].get_by_address(advcl_index[0])["word"]
    #         else:
    #             return root_node["word"]
    #     elif head_node["rel"] == "dep":
            # ccomp_index = root_node_dependencies["ccomp"]
            # xcomp_index = root_node_dependencies["xcomp"]
            
    #         if ccomp_index:
    #             return question_graph["dep"].get_by_address(ccomp_index[0])["word"]
    #         elif xcomp_index:
    #             second_xcomp = question_graph["dep"].get_by_address(xcomp_index[0])["deps"]["advcl"]

    #             if second_xcomp:
    #                 # print(">>> 2nd xcomp", question_graph["dep"].get_by_address(second_xcomp[0]))
    #                 return question_graph["dep"].get_by_address(second_xcomp[0])["word"]
    #             else:
    #                 # JJ code
    #                 # xcomp_node = question_graph["dep"].get_by_address(xcomp_index[0])
                    
    #                 # if xcomp_node and xcomp_node["tag"] == "JJ":
    #                 #     return question_graph["dep"].get_by_address(xcomp_index[0]-1)["word"]
    #                 # else:
    #                 return question_graph["dep"].get_by_address(xcomp_index[0])["word"]
    #         else:
    #             return root_node["word"]
    #     elif head_node["rel"] == "nsubj":
    #         xcomp_index = root_node_dependencies["xcomp"]

    #         if xcomp_index:
    #             return question_graph["dep"].get_by_address(xcomp_index[0])["word"]
    #         else:
    #             return root_node["word"]
    #     elif head_node["rel"] == "nsubjpass":
    #         return root_node["word"]
    #     elif head_node["rel"] == "root":
    #         pass
    # elif head_node["tag"] == "WDT":
    #     # print("WDT")
    #     # print(">>> head node: ", head_node)
    #     # print(">>> root deps", root_node_dependencies)
    #     # print(">>> root node", root_node)
    #     # print("@@@@@@")
    #     if head_node["rel"] == "nsubj":
    #         xcomp_index = root_node_dependencies["xcomp"]
            
    #         if xcomp_index:
    #             return question_graph["dep"].get_by_address(xcomp_index[0])["word"]
    #         else:
    #             return root_node["word"]
    #     elif head_node["rel"] == "det":
    #         return root_node["word"]
    # else:
    #     print(">>>", head_node["rel"])
    #     print(">>> head node: ", head_node)
    #     print(">>> root deps", root_node_dependencies)
    #     print(">>> root node", root_node)


# print(question_graph["dep"].root)
# print(question_graph["dep"].contains_address(1))
# print(question_graph["dep"].get_by_address(1))
# print(question_graph["dep"].left_children(2))
# print(question_graph["dep"].right_children(1))
# print(question_graph["dep"].top_relation_label)
# print(question_graph["dep"].top_relation_label)


def what_find_question_verb(question_graph):
    head_node = question_graph["dep"].nodes[1] # first word in question
    # root is the word all the dependencies stem from
    root_node = question_graph["dep"].root ## returns the {root node}
    root_node_dependencies = root_node["deps"]
    # print(root_node_dependencies)

    if root_node:
        if root_node["word"].lower() == "what":
            verbs = {}
            root_index = root_node_dependencies["root"]
            nsubj_index = root_node_dependencies["nsubj"]

            if root_index:
                index_node = question_graph["dep"].get_by_address(root_index[0])
                nsubj = index_node["deps"]["nsubj"]

                verbs["first_verb"] = index_node["word"]
                verbs["second_verb"] = question_graph["dep"].get_by_address(nsubj[0])["word"]

            if nsubj_index:
                nsubj_node = question_graph["dep"].get_by_address(nsubj_index[0])
                dobj_index = nsubj_node["deps"]["dobj"]
                nsubj_index = nsubj_node["deps"]
                
                if root_node["deps"]["cop"]:
                    verbs = None

                if dobj_index:
                    verbs["first_verb"] = nsubj_node["word"]
                    verbs["second_verb"] = question_graph["dep"].get_by_address(dobj_index[0])["word"]
            
            return verbs
        else:
            ccomp_index = root_node_dependencies["ccomp"]
            xcomp_index = root_node_dependencies["xcomp"]
            advcl_index = root_node_dependencies["advcl"]
            verbs = {"first_verb": root_node["word"]}

            if ccomp_index:
                c_node = question_graph["dep"].get_by_address(ccomp_index[0])
                verbs["second_verb"] = c_node["word"]
            
            if xcomp_index:
                x_node = question_graph["dep"].get_by_address(xcomp_index[0])
                verbs["second_verb"] = x_node["word"]
            
            if advcl_index:
                ad_node = question_graph["dep"].get_by_address(advcl_index[0])
                verbs["second_verb"] = ad_node["word"]
            return verbs
    else:
        return None


def what_find_question_subject(question_graph):
    head_node = question_graph["dep"].nodes[1] # first word in question
    # root is the word all the dependencies stem from
    root_node = question_graph["dep"].root ## returns the {root node}
    root_node_dependencies = root_node["deps"]
    # print(root_node_dependencies)

    subjects = {}
    index = root_node_dependencies["root"]
    nsubj_index = root_node_dependencies["nsubj"]
    nmod_index = root_node_dependencies["nmod"]

    if index:
        index_node = question_graph["dep"].get_by_address(index[0])
        nsubj = index_node["deps"]["nsubj"]
        compound = question_graph["dep"].get_by_address(nsubj[0])["deps"]["compound"]
        subjects["left"] = question_graph["dep"].get_by_address(compound[0])["word"]


    if nsubj_index:
        # print("<*****>>>>")
        nsubj_node = question_graph["dep"].get_by_address(nsubj_index[0])
        amod_index = nsubj_node["deps"]["amod"]
        nmod_poss_index = nsubj_node["deps"]["nmod:poss"]
        dobj_index = nsubj_node["deps"]["dobj"]

        if amod_index:
            subjects["left"] = question_graph["dep"].get_by_address(amod_index[0])["word"]
        if nmod_poss_index:
            subjects["left"] = question_graph["dep"].get_by_address(nmod_poss_index[0])["word"]
        if dobj_index:
            subjects["left"] = question_graph["dep"].get_by_address(dobj_index[0])["word"]
        
        if root_node["deps"]["cop"]:
            subjects["right"] = nsubj_node["word"]
        # elif nsubj_node["word"].lower() != "what":
        #     subjects["left"] = nsubj_node["word"]
        

    if nmod_index:
        nmod_node = question_graph["dep"].get_by_address(nmod_index[0])
        compound = nmod_node["deps"]["compound"]
        
        if compound:
            subjects["right"] = question_graph["dep"].get_by_address(compound[0])["word"]
        else:
            subjects["right"] = nmod_node["word"]

    if len(subjects) > 0:
        return subjects
    else:
        # question has no subject
        return None



    # head_node = question_graph["dep"].nodes[1] # first word in question
    # # root is the word all the dependencies stem from
    # root_node = question_graph["dep"].root ## returns the {root node}
    # root_node_dependencies = root_node["deps"]

    # if head_node["tag"] == "WP":
    #     nsubj_index = root_node_dependencies["nsubj"]
    #     if nsubj_index:
    #         return question_graph["dep"].get_by_address(nsubj_index[0])["word"]
    # elif head_node["tag"] == "WDT":
    #     nsubj_index = root_node_dependencies["nsubj"]
    #     nmod_index = root_node_dependencies["nmod"]
    #     dobj_index = root_node_dependencies["dobj"]
        
    #     # print(">>> root deps", root_node_dependencies)
    #     # print(">>> root node", root_node)
    #     if nsubj_index:
    #         amod_index = question_graph["dep"].get_by_address(nsubj_index[0])["deps"]["amod"]

    #         if amod_index:
    #             return question_graph["dep"].get_by_address(amod_index[0])["word"]
    #         elif nmod_index:
    #             return question_graph["dep"].get_by_address(nmod_index[0])["word"]
    #         elif dobj_index:
    #             return question_graph["dep"].get_by_address(dobj_index[0])["word"]
    #         else:
    #             return question_graph["dep"].get_by_address(nsubj_index[0])["word"]


def did_find_question_verb(question_graph):
    sub_graph = find_main(question_graph["dep"])
    # dependency_relation_keys = [x for x in sub_graph["deps"].keys()]

    return sub_graph["word"]


def did_find_question_subject(question_graph):
    sub_graph = find_main(question_graph["dep"])
    nsubj = find_node_by_address(sub_graph["deps"]["nsubj"][0], question_graph["dep"])
    
    root_node = question_graph["dep"].root ## returns the {root node}
    root_node_dependencies = root_node["deps"]

    xcomp_index = root_node_dependencies["xcomp"]
    dobj_index = root_node_dependencies["dobj"]

    if xcomp_index:
        return question_graph["dep"].get_by_address(xcomp_index[0])["word"]
    elif dobj_index:
        amod_index = question_graph["dep"].get_by_address(dobj_index[0])["deps"]["amod"]

        return question_graph["dep"].get_by_address(dobj_index[0])["word"]
    else:
        return nsubj["word"]
