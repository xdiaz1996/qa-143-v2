#!/usr/bin/env python


import sys, nltk, operator, re
from qa_engine.base import QABase
from qa_engine.score_answers import main as score_answers
import tsv
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import wordnet

exceptions = ['happen', 'travel', 'graze']
pronouns = ["I", "he", "He", "she", "She", "my", "My", "His", "his","hers", "Hers", "it", "It", "bull", "soup"]
# See if our pattern matches the current root of the tree
def matches(pattern, root):
    # Base cases to exit our recursion
    # If both nodes are null we've matched everything so far
    if root is None and pattern is None: 
        return root
        
    # We've matched everything in the pattern we're supposed to (we can ignore the extra
    # nodes in the main tree for now)
    elif pattern is None:                
        return root
        
    # We still have something in our pattern, but there's nothing to match in the tree
    elif root is None:                   
        return None

    # A node in a tree can either be a string (if it is a leaf) or node
    plabel = pattern if isinstance(pattern, str) else pattern.label()
    rlabel = root if isinstance(root, str) else root.label()

    # If our pattern label is the * then match no matter what
    if plabel == "*":
        return root
    # Otherwise they labels need to match
    elif plabel == rlabel:
        # If there is a match we need to check that all the children match
        # Minor bug (what happens if the pattern has more children than the tree)
        for pchild, rchild in zip(pattern, root):
            match = matches(pchild, rchild) 
            if match is None:
                return None 
        return root
    
    return None

def findVerbs(que):
    quesent = nltk.sent_tokenize(que)
    quewords = [nltk.word_tokenize(sent) for sent in quesent]
    quetagged = nltk.pos_tag(quewords[0])
    lmtzr = WordNetLemmatizer()
    verbs = []
    for word in quetagged:
        checkv = ""
        checkv = word[1]
        if checkv[:2] == "VB":
            word = lmtzr.lemmatize(word[0],"v")
            if (word == "be" or word == "do"):# no need to find syns of what and did
                verbs.append(word)
                continue
            wordsyns = wordnet.synsets(word)
            for syn in wordsyns:
                syno = syn.lemmas()[0].name()
                if syno == "feed":
                    syno = "graze"
                if syno in verbs:
                    continue
                verbs.append(syno)
        checkv = word[0]
        if checkv[-3:] == 'ing':
            verbs.append(lmtzr.lemmatize(word[0],"v"))
        elif checkv[-2:] == 'ed':
            verbs.append(lmtzr.lemmatize(word[0],"v"))
        elif checkv in exceptions:
            verbs.append(lmtzr.lemmatize(word[0],"v"))
    if verbs == []:
        return 'oops'
    else:
        return verbs

def findSubjs(que):
    quesent = nltk.sent_tokenize(que)
    quewords = [nltk.word_tokenize(sent) for sent in quesent]
    quetagged = nltk.pos_tag(quewords[0])
    lmtzr = WordNetLemmatizer()
    subjs = []
    for word in quetagged:
        checkv = ""
        checkv = word[0]
        if checkv[-3:] == 'ing':
            continue
        if checkv[-2:] == 'ed':
            continue
        if checkv in exceptions:
            continue
        checkv = word[1]
        if checkv[:2] == "NN":
            subjs.append(lmtzr.lemmatize(word[0], "n"))
        if (checkv[:3] == "PRP"):
            subjs.append(word[0])
        checkv = word[0]
        if checkv in pronouns:
            subjs.append(word[0])
        
    if subjs == []:
        return 'oops'
    else:
        return subjs

def findCandidates(verbs, subjs, sch_story):
    candsent = nltk.sent_tokenize(sch_story)
    vc = len(verbs)
    sc = len(subjs)
    lmtzr = WordNetLemmatizer()
    candidates = []
    i = 0
    for sent in candsent:
        vf = 0
        sf = 0
        words = nltk.word_tokenize(sent)
        print("\n")
        print(format(sent))
        for word in words:
            if word[:1] == '.':
                word = word[1:]
            if word in pronouns:
                print(word+" was in sujbs")
                sf +=1
                continue
            wordsl = lmtzr.lemmatize(word, "n")
            if wordsl in subjs:
                print(wordsl+" was in subjs")
                sf += 1
                continue
            wordl = lmtzr.lemmatize(word, "v")
            if wordl in verbs:
                print(wordl+" was in verbs")
                vf += 1
        if (vf >= 1 and sf >= 1):
            candidates.append(i)
        elif (vf >= 1 and sf == "oops"):
            candidates.append("found")
        i += 1
    return candidates

def pattern_matcher(pattern, tree):
    for subtree in tree.subtrees():
        node = matches(pattern, subtree)
        if node is not None:
            return node
    return None

if __name__ == '__main__':

    driver = QABase()
    q = driver.get_question("fables-02-2")
    version = q["type"]
    stry = False
    story = driver.get_story(q["sid"])
    # get the subj and verb of the question
    que = (q["text"])
    quetype = ""
    if que[1:5] == "here":
        quetype = "where"
    elif que[1:3] == "ho":
        quetype = "who"
    print(quetype)
    print(que)
    verbs = findVerbs(que)
    #was and do aren't very useful verbs if others are present, if more than 1 remove be
    better = []
    if len(verbs) > 1:
        for verb in verbs:
            if verb == 'be':
                continue
            elif verb == 'do':
                continue
            else:
                better.append(verb)
        verbs = better
    subjs = findSubjs(que)
    print("subjs: "+format(subjs))
    print("verbs: "+format(verbs))
    # put the sch_par together into a story to look over

    par_len = len(story["sch_par"])
    i = 0
    par_story = ""
    while i < par_len and version != "Story | Sch":
        a = " ".join(story["sch_par"][i].leaves())
        a = str(a[:-2])+"."
        par_story += " "+a
        i += 1
    par_story = par_story[1:]
    if par_story == "":
        stry = True
        par_len = len(story["story_par"])
        i = 0
        par_story = ""
        while i < par_len:
            a = " ".join(story["story_par"][i].leaves())
            a = str(a[:-2])+"."
            par_story += " "+a
            i += 1
        par_story = par_story[1:]
    print(format(par_story))
    #now have the story to look through, pass it with verbs and subjs
    candidates = findCandidates(verbs, subjs, par_story)
    print(format(candidates))

    #can now iterate over candidates and try to get the answer
    answers = []
    tree = ""
   # if stry == True:
    for cand in candidates:
        if stry == True:
            tree = story["story_par"][cand]
        else:
            tree = story["sch_par"][cand]
        print(format(tree))
        if quetype == "where":
            pattern = nltk.ParentedTree.fromstring("(VP)")#for where
        if quetype == "who":
            pattern = nltk.ParentedTree.fromstring("(NP)")#for who
            pattern2 = nltk.ParentedTree.fromstring("(VP)")#for who
        if quetype == "who":
            subtree = pattern_matcher(pattern, tree)
            subtreetwo = pattern_matcher(pattern2, tree)
        else:
            subtree = pattern_matcher(pattern, tree)
        if quetype == "who":
            pattern = nltk.ParentedTree.fromstring("(NP)")#for who
        if quetype == "where":
            pattern = nltk.ParentedTree.fromstring("(PP)")#for where
        if quetype == "who":
            subtree2 = pattern_matcher(pattern, subtree)
            subtree3 = pattern_matcher(pattern, subtreetwo)
        else:
            subtree2 = pattern_matcher(pattern, subtree)
        if subtree2 == None:
            continue
        if quetype == "who":
            if subtree3 == None:
                continue
        if quetype == "where":
            answers.append(" ".join(subtree2.leaves()))
        else:
            answers.append(" ".join(subtree2.leaves()))
            answers.append(" ".join(subtree3.leaves()))
        print(format(answers))

        #pattern = nltk.ParentedTree.fromstring("(VP)")

    # # Match our pattern to the tree  
        #subtree = pattern_matcher(pattern, tree)
    # print(" ".join(subtree.leaves()))

    # create a new pattern to match a smaller subset of subtree
        #pattern = nltk.ParentedTree.fromstring("(PP)")

    # Find and print the answer
        #subtree2 = pattern_matcher(pattern, subtree)
        #if subtree2 == None:
        #    continue
        #else:
        #    answers.append(" ".join(subtree2.leaves()))
