verbs = {
	"arise" : {
		"present_3rd" : "arises",
		"past_3rd" : "arose",
		"past" : "arisen"
	},
	"be" : {
		"present_3rd" : "is",
		"past_3rd" : ["was, were"],
		"past" : "been"
	},
	"bear" : {
		"present_3rd" : "bears",
		"past_3rd" : "bore",
		"past" : "borne"
	},
	"begin" : {
		"present_3rd" : "begins",
		"past_3rd" : "began",
		"past" : "begun"
	},
	"bite" : {
		"present_3rd" : "bites",
		"past_3rd" : "bit",
		"past" : ["bitten, bit"]
	},
	"blow" : {
		"present_3rd" : "blows",
		"past_3rd" : "blew",
		"past" : "blown"
	},
	"break" : {
		"present_3rd" : "breaks",
		"past_3rd" : "broke",
		"past" : "broken"
	},
	"bring" : {
		"present_3rd" : "brings",
		"past_3rd" : "brought",
		"past" : "brought"
	},
	"buy" : {
		"present_3rd" : "buys",
		"past_3rd" : "bought",
		"past" : "bought"
	},
	"catch" : {
		"present_3rd" : "catches",
		"past_3rd" : "caught",
		"past" : "caught"
	},
	"choose" : {
		"present_3rd" : "chooses",
		"past_3rd" : "chose",
		"past" : "chosen"
	},
	"come" : {
		"present_3rd" : "comes",
		"past_3rd" : "came",
		"past" : "come"
	},
	"creep" : {
		"present_3rd" : "creeps",
		"past_3rd" : "crept",
		"past" : "crept"
	},
	"dive" : {
		"present_3rd" : "dives",
		"past_3rd" : ["dived, dove"],
		"past" : "dived"
	},
	"do" : {
		"present_3rd" : "does",
		"past_3rd" : "did",
		"past" : "done"
	},
	"drag" : {
		"present_3rd" : "drags",
		"past_3rd" : "dragged",
		"past" : "dragged"
	},
	"draw" : {
		"present_3rd" : "draws",
		"past_3rd" : "drew",
		"past" : "drawn"
	},
	"dream" : {
		"present_3rd" : "dreams",
		"past_3rd" : ["dreamed, dreamt"],
		"past" : "dreamt"
	},
	"drink" : {
		"present_3rd" : "drinks",
		"past_3rd" : "drank",
		"past" : "drunk"
	},
	"drive" : {
		"present_3rd" : "drives",
		"past_3rd" : "drove",
		"past" : "driven"
	},
	"drown" : {
		"present_3rd" : "drowns",
		"past_3rd" : "drowned",
		"past" : "drowned"
	},
	"eat" : {
		"present_3rd" : "eats",
		"past_3rd" : "ate",
		"past" : "eaten"
	},
	"fall" : {
		"present_3rd" : "falls",
		"past_3rd" : "fell",
		"past" : "fallen"
	},
	"fight" : {
		"present_3rd" : "fights",
		"past_3rd" : "fought",
		"past" : "fought"
	},
	"fly" : {
		"present_3rd" : "flies",
		"past_3rd" : "flew",
		"past" : "flown"
	},
	"forget" : {
		"present_3rd" : "forgets",
		"past_3rd" : "forgot",
		"past" : "forgotten"
	},
	"forgive" : {
		"present_3rd" : "forgives",
		"past_3rd" : "forgave",
		"past" : "forgiven"
	},
	"freeze" : {
		"present_3rd" : "freezes",
		"past_3rd" : "froze",
		"past" : "frozen"
	},
	"get" : {
		"present_3rd" : "gets",
		"past_3rd" : "got",
		"past" : ["got, gotten"]
	},
	"give" : {
		"present_3rd" : "gives",
		"past_3rd" : "gave",
		"past" : "given"
	},
	"go" : {
		"present_3rd" : "goes",
		"past_3rd" : "went",
		"past" : "gone"
	},
	"grow" : {
		"present_3rd" : "grows",
		"past_3rd" : "grew",
		"past" : "grown"
	},
	"hang" : {
		"present_3rd" : "hangs",
		"past_3rd" : "hung",
		"past" : "hung"
	},
	"hide" : {
		"present_3rd" : "hides",
		"past_3rd" : "hid",
		"past" : "hidden"
	},
	"know" : {
		"present_3rd" : "knows",
		"past_3rd" : "knew",
		"past" : "known"
	},
	"lay" : {
		"present_3rd" : "lays",
		"past_3rd" : "laid",
		"past" : "laid"
	},
	"lead" : {
		"present_3rd" : "leads",
		"past_3rd" : "led",
		"past" : "led"
	},
	"lie" : {
		"present_3rd" : "lies",
		"past_3rd" : "lay",
		"past" : "lain"
	},
	"light" : {
		"present_3rd" : "lights",
		"past_3rd" : "lit",
		"past" : "lit"
	},
	"lose" : {
		"present_3rd" : "loses",
		"past_3rd" : "lost",
		"past" : "lost"
	},
	"prove" : {
		"present_3rd" : "proves",
		"past_3rd" : "proved",
		"past" : ["proved, proven"]
	},
	"ride" : {
		"present_3rd" : "rides",
		"past_3rd" : "rode",
		"past" : "ridden"
	},
	"ring" : {
		"present_3rd" : "rings",
		"past_3rd" : "rang",
		"past" : "rung"
	},
	"rise" : {
		"present_3rd" : "rises",
		"past_3rd" : "rose",
		"past" : "risen"
	},
	"run" : {
		"present_3rd" : "runs",
		"past_3rd" : "ran",
		"past" : "run"
	},
	"see" : {
		"present_3rd" : "sees",
		"past_3rd" : "saw",
		"past" : "seen"
	},
	"seek" : {
		"present_3rd" : "seeks",
		"past_3rd" : "sought",
		"past" : "sought"
	},
	"set" : {
		"present_3rd" : "sets",
		"past_3rd" : "set",
		"past" : "set"
	},
	"shake" : {
		"present_3rd" : "shakes",
		"past_3rd" : "shook",
		"past" : "shaken"
	},
	"sing" : {
		"present_3rd" : "sings",
		"past_3rd" : "sang",
		"past" : "sung"
	},
	"sink" : {
		"present_3rd" : "sinks",
		"past_3rd" : "sank",
		"past" : "sunk"
	},
	"sit" : {
		"present_3rd" : "sits",
		"past_3rd" : "sat",
		"past" : "sat"
	},
	"speak" : {
		"present_3rd" : "speaks",
		"past_3rd" : "spoke",
		"past" : "spoken"
	},
	"spring" : {
		"present_3rd" : "springs",
		"past_3rd" : "sprang",
		"past" : "sprung"
	},
	"steal" : {
		"present_3rd" : "steals",
		"past_3rd" : "stole",
		"past" : "stolen"
	},
	"sting" : {
		"present_3rd" : "stings",
		"past_3rd" : "stung",
		"past" : "stung"
	},
	"strike" : {
		"present_3rd" : "strikes",
		"past_3rd" : "struck",
		"past" : "struck"
	},
	"swear" : {
		"present_3rd" : "swears",
		"past_3rd" : "swore",
		"past" : "sworn"
	},
	"swim" : {
		"present_3rd" : "swims",
		"past_3rd" : "swam",
		"past" : "swum"
	},
	"swing" : {
		"present_3rd" : "swings",
		"past_3rd" : "swung",
		"past" : "swung"
	},
	"take" : {
		"present_3rd" : "takes",
		"past_3rd" : "took",
		"past" : "taken"
	},
	"tear" : {
		"present_3rd" : "tears",
		"past_3rd" : "tore",
		"past" : "torn"
	},
	"throw" : {
		"present_3rd" : "throws",
		"past_3rd" : "threw",
		"past" : "thrown"
	},
	"uses" : {
		"present_3rd" : "used",
		"past_3rd" : "used",
		"past" : "used"
	},
	"wake" : {
		"present_3rd" : "wakes",
		"past_3rd" : ["woke, waked"],
		"past" : ["woken, waked, woke"]
	},
	"wear" : {
		"present_3rd" : "wears",
		"past_3rd" : "wore",
		"past" : "worn"
	},
	"write" : {
		"present_3rd" : "writes",
		"past_3rd" : "wrote",
		"past" : "written"
	},
    "die": {
        "past" : "died"
    },
    "bore": {
        "past":"bored"
    }
}
