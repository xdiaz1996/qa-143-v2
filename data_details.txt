difficulty | question id | question

sorted questions

==================================================================================================
what
==================================================================================================
Easy blogs-03-1 What was Louisiana adversely affected by?
Easy blogs-03-2 What rolled in on Monday, September 1, in the morning?
Easy blogs-03-3 What started kicking up?
Easy blogs-03-7 What did the area of a neighborhood of the narrator have?
Medium blogs-03-12 What fell on the yard of a neighbor?
Medium blogs-03-14 What did the first tree fall onto?
Easy fables-01-2 What did the crow have in her beak?
Easy fables-01-4 What did the fox do to the cheese?
Easy fables-01-6 What did the crow feel?
Medium fables-01-8 What was the crow doing?
Medium fables-01-12 What did the fox say about the bird’s beauty?
Easy fables-02-6 What did the lion feign?
Easy fables-02-9 What did the lion want?
Easy fables-02-11 What did the lion fear?
Medium fables-02-12 What is ugly?
Medium fables-02-15 What did the lion advise the bull?
Medium fables-02-17 What did the lion decide?
Easy blogs-01-2 What is the summit meeting named?
Easy blogs-01-4 What did the people burn?
Easy blogs-01-7 What happened to a police car?
Easy blogs-01-9 What was burned?
Easy blogs-01-10 What happened to the police cars?
Easy blogs-01-11 What was fired?
Easy blogs-01-12 What was fired at the rioters?
Easy blogs-01-13 What was smashed?
Medium blogs-01-17 What did the people create?
Easy fables-04-2 What was the only fare provided?
Easy fables-04-5 What did the fox do to the soup?
Easy fables-04-7 What did the stork easily fitted the long bill into?
Medium fables-04-9 What was savoury?
Medium fables-04-11 What was the shape of the neck of the pitcher?
Medium fables-04-12 What was clever?
Medium fables-04-13 What was the soup served in?
Easy blogs-02-6 What did the narrator see?
Easy blogs-02-11 What did the narrator begin to wonder?
Easy blogs-02-12 What was in a group of trees that was above the narrator?
Easy blogs-04-3 What slipped off the deck's railing?
Easy blogs-04-4 What did the birds bath themselves in?
Easy blogs-04-7 What did I wish I had?
Medium blogs-04-14 What did we keep outside on the back deck?
Easy fables-03-3 What did the Eagle seize it with?
Easy fables-03-5 What did the Serpent spit?
Easy fables-03-7 What did the countryman decide?
Easy fables-03-9 What did the eagle spill?
Easy fables-03-11 What was heated?
Easy fables-03-12 What did the Serpent do in revenge?
Medium fables-03-17 What did the countryman own?
Medium fables-03-19 What did the countryman free the eagle from?
Medium fables-03-20 What did the Serpent spat?
Easy mc500.train.0.2 What did Ellen order?
Easy mc500.train.0.5 What makes Alyssa happy?
Easy mc500.train.0.9 What was the restaurant's special?
Easy mc500.train.0.10 What did Kristin have to eat?
Medium mc500.train.0.13 What did Ellen and Alyssa have to drink?
Medium mc500.train.0.14 What did the ladies do after dinner?
Easy mc500.train.23.2 What day was it?
Easy mc500.train.23.4 what did Andrew see by the door?
Easy mc500.train.23.7 What did Andrew and his dad read?
Easy mc500.train.23.10 What did Andrew feed the goldfish?
Easy mc500.train.23.12 What did Andrew and his dad eat?
Medium mc500.train.23.16 What  did Andrew's dad say about the newspaper?
Medium mc500.train.23.19 What were the animals at the circus doing?
Medium mc500.train.23.20 What did he bring to his dad?
Medium mc500.train.23.21 What did Andrew need to do before he could go to the circus?
Easy mc500.train.18.3 what did Kelly do after she woke up?
Easy mc500.train.18.4 What is big and beautiful?
Easy mc500.train.18.5 what did Kelly get?
Easy mc500.train.18.6 What did Kelly collect?
Easy mc500.train.18.10 What did Kelly want to give her mom?
Easy mc500.train.18.13 what size were the orange ones?
Easy mc500.train.18.14 what did Kelly hide?
Easy mc500.train.18.16 What time did Kelly's mother wake up?
Medium mc500.train.18.17 What did Kelly use to tie the flowers together?
Medium mc500.train.18.18 what did Kelly give her mom as a present?
Medium mc500.train.18.19 What was her baby sister's name?

==================================================================================================
who
==================================================================================================

Easy blogs-03-4 Who came to Baton Rouge yesterday to assess the damage?
Medium blogs-03-10 Who filled the container of some gasoline?
Medium blogs-03-11 Who had a group of oak trees?
Medium blogs-03-13 Who watched a large tree?
Medium blogs-03-15 Who stayed in an area of the home where we felt safest?
Easy fables-01-3 Who is the Story about?
Easy fables-01-7 Who flattered the crow?
Medium fables-01-10 Who set his wits to work?
Medium fables-01-11 Who tried to discover how to get the cheese?
Easy fables-02-1 Who was feeding in a meadow?
Easy fables-02-2 Who was persuaded by this flattery?
Easy fables-02-4 Who was grazing?
Easy fables-02-5 Who watched the bull?
Easy fables-02-7 Who is the story about?
Easy fables-02-8 Who was foolish?
Medium fables-02-13 Who salivated?
Medium fables-02-18 Who has horns?
Easy blogs-01-5 Who rebelled?
Easy blogs-01-6 Who created a riot?
Easy blogs-01-8 Who burned a police car?
Medium blogs-01-14 Who fired a bullet at the people?
Medium blogs-01-15 Who was tear gas fired at?
Easy fables-04-1 Who did the Fox invite to dinner?
Easy fables-04-4 Who is the story about?
Medium fables-04-10 Who failed to drink some broth?
Easy blogs-02-1 Who died?
Easy blogs-02-3 Who visited the place where he died?
Easy blogs-02-7 Who was snoring?
Easy blogs-02-8 Who crashed the motorbike?
Easy blogs-02-9 Who stayed with the young man?
Easy blogs-02-10 Who is the story about?
Medium blogs-02-15 Who broke the neck of the young man?
Medium blogs-02-16 Who decided to talk to the young man?
Easy blogs-04-1 Who approached the bowl?
Easy blogs-04-2 Who fell over the deck's railing?
Easy blogs-04-5 Who also comes to drink out of it?
Easy blogs-04-6 Who saw his one little paw hanging on?
Easy blogs-04-8 Who drinks out of it and bathes in it?
Easy fables-03-1 Who is the story about?
Easy fables-03-2 Who swooped down upon a Serpant?
Easy fables-03-4 Who came to the assistance of the Eagle?
Easy fables-03-8 Who spilled the drinking vessel?
Medium fables-03-13 Who seized it?
Medium fables-03-14 Who had its coils around him?
Medium fables-03-15 Who was a witness of the encounter?
Medium fables-03-16 Who came to the assistance of the Eagle?
Medium fables-03-18 Who succeeded in freeing him?
Medium fables-03-21 Who spilled its contents?
Easy mc500.train.0.3 who had soup?
Easy mc500.train.0.7 Who traded stories?
Easy mc500.train.0.8 Who went to the beach?
Easy mc500.train.23.11 Who did lots of tricks?
Medium mc500.train.23.18 Who went down the slide?
Medium mc500.train.23.22 Who ate the peanuts?

==================================================================================================
where
==================================================================================================

Easy mc500.train.23.3 Where did Andrew look?
Easy mc500.train.23.5 Where was the picture of the elephant?
Easy mc500.train.23.8 Where was the circus?
Easy mc500.train.23.13 Where did Andrew and his dad have fun?
Easy mc500.train.23.14 Where did Andrew and his dad go?
Medium mc500.train.23.17 Where was the newspaper?
Easy mc500.train.18.1 Where did Kelly go?
Easy mc500.train.18.7 Where were the best flowers?
Easy mc500.train.18.8 Where did Kelly hide the flowers?
Easy mc500.train.0.4 where did Alyssa travel from?
Easy mc500.train.0.11 Where did the girls go for dinner?
Easy mc500.train.0.12 Where did Alyssa meet her friends?
Easy blogs-03-5 Where was the storm occurring?
Easy blogs-03-6 Where did the narrator go back to in order to purchase a portable generator?
Easy blogs-03-9 Where did the narrator bring the family of the narrator to?
Easy fables-01-1 Where was the crow sitting?
Easy fables-01-5 Where was the fox standing?
Medium fables-01-9 Where was the cheese?
Easy fables-02-3 Where was the bull?
Easy blogs-01-3 Where did the protest happen?
Easy fables-04-3 Where was some soup served?
Easy blogs-02-2 Where did the young man die?
Easy blogs-02-5 Where did the narrator enter?
Medium blogs-02-14 Where did the young man crash the motorbike?
Easy blogs-04-9 Where do the birds literally line up on?
Medium blogs-04-10 Where did the narrator place the bowl?
Medium blogs-04-11 Where did the squirrel fall?
Easy fables-03-6 Where did the Serpent spit some poison?
Easy fables-03-10 Where where the contents of the horn spilled?

==================================================================================================
when X
==================================================================================================

Easy blogs-01-1 When did the G20 summit start?
Medium blogs-02-13 When did the young man die?
Easy mc500.train.23.1 When was Andrew bored?
Easy mc500.train.18.2 When did Kelly wake up?

==================================================================================================
why X
==================================================================================================
Easy blogs-03-8 Why did the narrator begin to be nervous?
Easy fables-02-10 Why did not the lion attack the bull?
Medium fables-02-14 Why did the lion salivate?
Medium fables-02-16 Why did the lion advise the bull to remove its horns?
Medium blogs-01-16 Why did the people protest?
Medium blogs-01-18 Why were the police in full riot gear?
Easy fables-04-6 Why did the fox begin to be hungry and begin to be helpless?
Easy fables-04-8 Why did the Fox sit by hungry and helpless?
Easy blogs-02-4 Why did the narrator decide to talk to the young man?
Medium blogs-04-12 Why did the narrator place the bowl on the deck?
Medium blogs-04-13 Why did the birds organize themselves?
Easy mc500.train.0.1 Why did Alyssa go to Miami?
Easy mc500.train.0.6 Why did they stay the night?
Medium mc500.train.18.9 Why was Kelly in a hurry?
Easy mc500.train.18.12 Why was Kelly in a hurry?
Medium mc500.train.18.20 Why did Kelly hide the flowers a second time?
Medium mc500.train.18.21 Why was Kelly collecting flowers?

==================================================================================================
how
==================================================================================================
Medium fables-01-13 How did the crow caw?
Easy mc500.train.18.11 How did Kelly feel when her mother woke up?
Easy mc500.train.23.6 How long had Andrew been watching TV?

==================================================================================================
Had
==================================================================================================

Easy mc500.train.23.9 Had Andrew ever been to the circus?

==================================================================================================
Which
==================================================================================================

Easy mc500.train.23.15 Which character in the story went down the slide?
Easy mc500.train.18.15 Which flower was mother's favorite?

==================================================================================================
Did
==================================================================================================

Medium mc500.train.0.15 Did Alyssa and her friends have a good time?
Medium mc500.train.0.16 Did Alyssa go swimming?

