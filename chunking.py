import nltk


def concatenate_chunks(grammer_rule, tagged_question, label, debug):
    regex_parser = nltk.RegexpParser(grammer_rule)
    chunks = regex_parser.parse(tagged_question)

    if debug:
        for leaf in chunks.subtrees():
            if leaf.label() == label:
                print("[*] DEBUG ({}): ".format(label), leaf.leaves())

    return " ".join([word for leaf in chunks.subtrees() if leaf.label() == label for word, tag in leaf.leaves()])


def get_chunks(question_type, tagged_question, label, debug=False):
    grammer_rule = None

    if question_type == "when":
        if label == "subject":
            grammer_rule = r"""
                    ADJ:     {<JJ.*>}
                    subject: <DT>?<ADJ>*{<NN.?>}
                """
        elif label == "verb":
            grammer_rule = r"""
                    verb: <NN.?>{<VB.*>}
                """
    elif question_type == "why":
        if label == "subject":
            grammer_rule = r"""
                    ADJ:     {<JJ.*>}
                    subject: <DT>?<ADJ>*{<NN.*>?}
                """
        elif label == "verb":
            grammer_rule = r"""
                    verb: <NN.?>{<VB.*>}
                """
    return concatenate_chunks(grammer_rule, tagged_question, label, debug)
