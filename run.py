import operator
from base import QABase
from score_answer import main as score_answers
from parsing_sentences import parse_sentence
from dependency import find_answer
from nltk.stem.wordnet import WordNetLemmatizer


def get_question_type(question):
    return question.lower().split(" ")[0]


def get_answer(question, story):
    """
    :param question: dict
    :param story: dict
    :return: str

    question is a dictionary with keys:
        dep -- A list of dependency graphs for the question sentence.
        par -- A list of constituency parses for the question sentence.
        text -- The raw text of story.
        sid --  The story id.
        difficulty -- easy, medium, or hard
        type -- whether you need to use the 'sch' or 'story' versions of the .
        qid  --  The id of the question.

    story is a dictionary with keys:
        story_dep -- list of dependency graphs for each sentence of the story version.
        sch_dep -- list of dependency graphs for each sentence of the sch version.
        sch_par -- list of constituency parses for each sentence of the sch version.
        story_par -- list of constituency parses for each sentence of the story version.
        sch --  the raw text for the sch version.
        text -- the raw text for the story version.
        sid --  the story id
    """
    # print(question["difficulty"], question["qid"], question["text"])

    # question
    question_type = get_question_type(question["text"])
    qgraph = question["dep"]
    answer = None

    # print question types
    # if question_type == "what":
    #     if question["difficulty"] == "Medium":
    #         print(question["text"])

    if question_type == "who":
        pass
    elif question_type == "what":
        # if question["difficulty"] == "Medium":
        sgraph = parse_sentence(question_type, question, story)

        if sgraph and qgraph:
            answer = find_answer(qgraph, sgraph)
            # print('\033[42m' + '\033[30m' + "[*] Answer: " + str(answer) + '\033[0m')
            print(answer)

        # pass
    elif question_type == "where":
        # sgraph = parse_sentence(question_type, question, story)
        pass
    elif question_type == "when":
        # sgraph = parse_sentence(question_type, question, story)

        # if sgraph and qgraph:
        #     answer = find_answer(qgraph, sgraph)
        #     print('\033[42m' + '\033[30m' + "[*] Answer: " + str(answer) + '\033[0m')

        pass
    elif question_type == "why":
        pass
    elif question_type == "how":
        pass
    elif question_type == "did":
        # sgraph = parse_sentence(question_type, question, story)

        # if sgraph and qgraph:
        #     answer = find_answer(qgraph, sgraph)
        #     print('\033[42m' + '\033[30m' + "[*] Answer: " + str(answer) + '\033[0m')

        pass
    elif question_type == "had":
        pass
    elif question_type == "which":
        pass

    return answer


class QAEngine(QABase):
    @staticmethod
    def answer_question(question, story):
        answer = get_answer(question, story)
        return answer


def run_qa(save):
    QA = QAEngine()
    
    QA.run()
    if save:
        QA.save_answers()


def main(score=False, save=False):
    run_qa(save)
    if score:
        score_answers()

if __name__ == "__main__":
    main()
