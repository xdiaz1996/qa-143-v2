import nltk
from parsing_sentences import parse_sentence
from dependency import find_answer

def q_what():
    sgraph = parse_sentence(question_type, question, story)

    if sgraph and qgraph:
        answer = find_answer(qgraph, sgraph)
        # print('\033[42m' + '\033[30m' + "[*] Answer: " + str(answer) + '\033[0m')

if __name__ == "__main__":
    q_what()
